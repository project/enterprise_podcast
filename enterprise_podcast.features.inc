<?php
/**
 * @file
 * enterprise_podcast.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function enterprise_podcast_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function enterprise_podcast_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function enterprise_podcast_node_info() {
  $items = array(
    'enterprise_podcast' => array(
      'name' => t('Podcast'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Podcast name'),
      'help' => '',
    ),
  );
  return $items;
}
